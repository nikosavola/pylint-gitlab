#
# Copyright 2019 Stephan Müller
#
# Licensed under the MIT license

"""Module ``pylint_gitlab``."""

from pylint_gitlab.reporter import GitlabCodeClimateReporter, GitlabPagesHtmlReporter


def register(linter):
    """Register the reporter classes with the linter."""
    linter.register_reporter(GitlabCodeClimateReporter)
    linter.register_reporter(GitlabPagesHtmlReporter)

# Changelog

## Unreleased

## 1.0.0

### Added

* Add support for Python 3.10 [!9](https://gitlab.com/smueller18/pylint-gitlab/-/merge_requests/9)

### Changed

* Change fingerprint algorithm from md5 to sha256 [#10](https://gitlab.com/smueller18/pylint-gitlab/-/issues/10)
* Pipenv update [!9](https://gitlab.com/smueller18/pylint-gitlab/-/merge_requests/9)

### Removed

* Drop support for Python 3.6 [!9](https://gitlab.com/smueller18/pylint-gitlab/-/merge_requests/9)
